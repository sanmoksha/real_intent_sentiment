import { Component, Output, EventEmitter, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-summarydialog',
  templateUrl: './summarydialog.component.html',
  styleUrls: ['./summarydialog.component.scss']
})
export class SummarydialogComponent implements OnInit {

  @Output() submitClicked = new EventEmitter<any>();

  constructor(public summarydialogRef: MatDialogRef<SummarydialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      summarydialogRef.disableClose = true;
     }

  ngOnInit() {
    console.log(this.data)
  }

  saveMessage() {
    console.log(this.data)
    this.submitClicked.emit(this.data);
    this.summarydialogRef.close();
  }

  close(data) {
    this.summarydialogRef.close(data);
  }

}
