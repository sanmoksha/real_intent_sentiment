import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummarydialogComponent } from './summarydialog.component';

describe('SummarydialogComponent', () => {
  let component: SummarydialogComponent;
  let fixture: ComponentFixture<SummarydialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummarydialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummarydialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
