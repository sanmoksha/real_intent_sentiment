import { Component, Output, EventEmitter, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-disposotiondialog',
  templateUrl: './disposotiondialog.component.html',
  styleUrls: ['./disposotiondialog.component.scss']
})
export class DisposotiondialogComponent implements OnInit {

  favoriteSeason: string;
  seasons: any;

  @Output() submitClicked = new EventEmitter<any>();

  constructor(public dispositiondialogRef: MatDialogRef<DisposotiondialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    dispositiondialogRef.disableClose = true;
  }

  ngOnInit() {
    this.seasons = this.data;
  }

  saveMessage(selected) {
    console.log(selected,"Selected")
    // this.submitClicked.emit(this.favoriteSeason);
    // this.dispositiondialogRef.close();
  }

}
