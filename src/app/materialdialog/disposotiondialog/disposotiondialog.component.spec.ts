import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisposotiondialogComponent } from './disposotiondialog.component';

describe('DisposotiondialogComponent', () => {
  let component: DisposotiondialogComponent;
  let fixture: ComponentFixture<DisposotiondialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisposotiondialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisposotiondialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
