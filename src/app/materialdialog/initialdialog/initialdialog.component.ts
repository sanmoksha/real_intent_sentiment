import { Component, OnInit, HostListener, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-initialdialog',
  templateUrl: './initialdialog.component.html',
  styleUrls: ['./initialdialog.component.scss']
})
export class InitialdialogComponent implements OnInit {

  cardheader: string;
  constructor(public initialdialogRef: MatDialogRef<InitialdialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any){
    initialdialogRef.disableClose = true;
  }

  @HostListener('window:keyup.esc') onKeyUp() {
    this.initialdialogRef.close();
  }

  ngOnInit() {
    console.log(this.data)
    this.cardheader = this.data;
  }

  close(data) {
    this.initialdialogRef.close(data);
  }

}
