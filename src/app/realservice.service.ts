import { Injectable } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RealserviceService {

  categorySelected: any;

  domain: String ='';
 //  domain: String = 'http://192.168.11.19:8081'  previous one
//  domain : String = 'http://192.168.11.19:7200'
// domain : String = 'http://10.7.138.96:8081'
 

 // RD host : 192.168.11.18:33893  

 categorylist: string = '/platform/category/';
 loginUrl: string = '/user/session/detail';
 logoutUrl: string = '/logout';

  webSocketEndPoint: string = this.domain + '/aa-config/intentdata';

  MetaData_topic: string = "/queue/reply.callmeta";
  Transcript_topic: string = "/queue/reply.transcription";
  Alert_topic: string = "/queue/reply.alert";
  Summary_topic: string = "/queue/reply.summary";
  Disposition_topic: string = "/queue/reply.calldisposition";
  Sentiment_topic: string = "/queue/reply.sentiment";

  stompClient: any;
  file: File;

  constructor(private http: HttpClient) {

  }

  // Observable string sources
  private componentMetaData = new Subject<any>();

  private componentErrorCallTranscript = new Subject<any>();
  private componentMethodCallAlert = new Subject<any>();
  private componentMethodCallSummary = new Subject<any>();
  private componentErrorCallDisposition = new Subject<any>();
  private componentErrorCallSentiment = new Subject<any>();

  // Observable string streams
  componentMethodMetaData$ = this.componentMetaData.asObservable();

  componentMethodCalledAlert$ = this.componentMethodCallAlert.asObservable();
  componentMethodCalledSummary$ = this.componentMethodCallSummary.asObservable();
  componentErrorCalledDisposition$ = this.componentErrorCallDisposition.asObservable();
  componentErrorCalledCallSentiment$ = this.componentErrorCallSentiment.asObservable();
  componentErrorCalledCallTranscription$ = this.componentErrorCallTranscript.asObservable();

 
  _connect(crmid,orgname) {
    console.log("Initialize WebSocket Connection");
    let ws = new SockJS(this.webSocketEndPoint);
     this.stompClient = Stomp.over(ws);
    const _this = this;
     
    _this.stompClient.connect({}, function (frame) {

      _this.stompClient.subscribe(_this.MetaData_topic.replace('.','.'+crmid+'.'), function (sdkEvent) {
        _this.componentMetaData.next(sdkEvent);
      });

      _this.stompClient.subscribe(_this.Transcript_topic.replace('.','.'+crmid+'.'), function (sdkEvent) {
        _this.componentErrorCallTranscript.next(sdkEvent);
      });
      
      _this.stompClient.subscribe(_this.Disposition_topic.replace('.','.'+crmid+'.'), function (sdkEvent) {
        _this.componentErrorCallDisposition.next(sdkEvent);
      });
      _this.stompClient.subscribe(_this.Summary_topic.replace('.','.'+crmid+'.'), function (sdkEvent) {
        _this.componentMethodCallSummary.next(sdkEvent);
      });
      _this.stompClient.subscribe(_this.Sentiment_topic.replace('.','.'+crmid+'.'), function (sdkEvent) {
        _this.componentErrorCallSentiment.next(sdkEvent);
      });
      _this.stompClient.subscribe(_this.Alert_topic.replace('.','.'+crmid+'.'), function (sdkEvent) {
        _this.componentMethodCallAlert.next(sdkEvent);
      });

     // if(this.file!=null)
      _this._uploadFile(crmid,orgname);
    }, this.errorCallBack);
 
  };

  _disconnect() {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
      console.log("Disconnected");
    }

  }

  errorCallBack(error) {
    console.log('web socket error ', error);
    alert(error);

  }

  setFile(file) {
    this.file = file;
  }

  getSessionId(client) {
    const url = client.ws._transport.url;
    const urlArray = url.split('/');
    const index = urlArray.length - 2;
    const sessionId = urlArray[index];

    return sessionId;
  }

  _uploadFile(crmid,orgname) {
    if(this.file == null)
    console.log('file is null');
     const formData = new FormData();
    formData.append('agentId', crmid);
    formData.append('customerId', 'Stanley Wilson');
    formData.append('file', this.file);
    formData.append('callId', this.getSessionId(this.stompClient));

    this.http.post<any>(this.domain+'/aa-config/offline/data?organization='+orgname+'&category=Sales' , formData).subscribe(
      (res) => {
        console.log('post res succ ',res);
      
      },
      (err) => {console.log('post res error ',err)
    }
    );
  }

  sessiondetails(){
    return this.http.get(this.loginUrl);
  }

  ListofCategories(orgname){
    return this.http.get(this.categorylist + orgname);
  }

  logoutinfo(){
    // Before logging out please delete the cache data.
    return this.http.get(this.logoutUrl);
 }

  saveSummary(summary, callid) {
    console.log("S S",summary, callid)
    const formData = new FormData();
    formData.append('callid', callid);
    formData.append('summary', summary);
    this.http.put<any>(this.domain+'/aa-config/agent/summary?callId='+ callid, formData).subscribe(
      (res) => {
        console.log('save summary ',res);
      },
      (err) => {console.log('save summary error ',err)
    }
    );
  }

}
