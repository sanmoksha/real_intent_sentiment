import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RealComponentNew } from './real2/real.component';
import { LiveCallSummayComponent } from './live-call-summay/live-call-summay.component';

const routes: Routes = [
  { path : '' , redirectTo : 'live', pathMatch : 'full'},
  { path : 'real', component : RealComponentNew },
  { path : 'live', component : LiveCallSummayComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
