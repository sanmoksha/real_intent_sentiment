import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faFile, faUserCircle, faExpand } from '@fortawesome/free-solid-svg-icons'
import { HttpClientModule } from '@angular/common/http';
import { LiveCallSummayComponent } from './live-call-summay/live-call-summay.component'; 
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RealComponentNew } from './real2/real.component';
import {NoSanitizePipe} from './no-sanitize.pipe';
import {MatDialogModule, MatRadioModule } from "@angular/material";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material/select';
// import { DatePipe } from '@angular/common';

import { InitialdialogComponent } from './materialdialog/initialdialog/initialdialog.component';
import { SummarydialogComponent } from './materialdialog/summarydialog/summarydialog.component';
import { DisposotiondialogComponent } from './materialdialog/disposotiondialog/disposotiondialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LiveCallSummayComponent,
    NoSanitizePipe,
    RealComponentNew,
    InitialdialogComponent,
    SummarydialogComponent,
    DisposotiondialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    FontAwesomeModule,
    HttpClientModule,
    DragDropModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatRadioModule,
    MatSelectModule 
  ],
  providers: [],
  entryComponents: [InitialdialogComponent,SummarydialogComponent,DisposotiondialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor() {
    // Add an icon to the library for convenient access in other components
    library.add(faFile,faUserCircle,faExpand);
  }
}
