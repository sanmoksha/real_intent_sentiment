import { Component, Input, OnChanges } from '@angular/core';
import { RealserviceService } from '../realservice.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnChanges {
 
  
  @Input() agentName: string;
  receivedata : any;

  constructor(private realservice: RealserviceService) {  }

  elem = document.documentElement;
  mozFullScreenElement?: boolean;
  webkitFullscreenElement?: boolean;
  public agentname: string

  togglebtn : boolean = false;
  // val : number = 1;
  val : boolean = true;
  
  ngOnChanges() {
    this.agentname = this.agentName; 
  }

  Togglebtnshow(){
    this.togglebtn == true ? this.togglebtn = false : this.togglebtn = true;
  }

  commontoggle(){
    this.val == true ? (this.Togglescreen() , this.togglebtn = false , this.val = false) : 
        (this.closeFullscreen() , this.togglebtn = false , this.val = true)
  }
  
  Togglescreen(){
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    } 
    // else if (this.elem.mozRequestFullScreen) { /* Firefox */
    //   this.elem.mozRequestFullScreen();
    // } else if (this.elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
    //   this.elem.webkitRequestFullscreen();
    // } 
    // else if (this.elem.msRequestFullscreen) { /* IE/Edge */
    //   this.elem.msRequestFullscreen();
    // }
  }
  
  closeFullscreen(){
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } 
    }

    loggedout(){
      this.realservice.logoutinfo().subscribe((data: any[]) => {
        console.log("logoutinfo",data);
      })
      window.location.reload();
    }

    
}
