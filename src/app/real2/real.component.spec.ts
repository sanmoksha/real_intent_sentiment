import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealComponentNew } from './real.component';

describe('RealComponentNew', () => {
  let component: RealComponentNew;
  let fixture: ComponentFixture<RealComponentNew>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealComponentNew ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealComponentNew);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

 /*  it('should create', () => {
    expect(component).toBeTruthy();
  }); */
});
