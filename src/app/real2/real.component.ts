import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import wavesurfer from 'wavesurfer.js';
import { RealserviceService } from '../realservice.service';
import { PushnotificationsService } from '../pushnotifications.service';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InitialdialogComponent } from '../materialdialog/initialdialog/initialdialog.component';
import { SummarydialogComponent } from '../materialdialog/summarydialog/summarydialog.component';
import { DisposotiondialogComponent } from '../materialdialog/disposotiondialog/disposotiondialog.component';

import { FormBuilder, FormGroup,FormControl,Validators } from '@angular/forms';

@Component({
  selector: 'app-real2',
  templateUrl: './real.component.html',
  styleUrls: ['./real.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RealComponentNew implements OnInit {
  copiedsummarydata: String;
  summaryEditclicked = false;
  summarycardbtns: boolean = false;
  dispositionEditclicked: boolean = false;
  summarytimestamplabel = false;
  distimestamplabel = false;
  dispFilterVal = [];
  blockdropdown = true;
  patientCategory: FormGroup;

  seasons = [
    { header: 'Call Type', items: ['Hotel Booking', 'Insurance'], selected: 'Hotel Booking', id: 1 },
    { header: 'Service', items: ['Deluxe', 'Normal', 'Benefit Verification'], selected: 'Deluxe', id: 2 },
    { header: 'Issue', items: ['City view', 'Sea View', 'Multi-Bed',
      'Single Bed', 'Claim'], selected: 'City view', id: 3 },
    { header: 'Status', items: ['Closed', 'Booked'], selected: 'Closed', id: 4 }
  ]

  ArrowDown = 'fa fa-angle-double-down';
  ArrowUp = 'fa fa-angle-double-up';
  ArrowRight = 'fa fa-angle-double-right';
  ArrowLeft = 'fa fa-angle-double-left';

  positions = [true, true, false, false, false, false];
  seltitle;
  copyindex;
  count: number = 1;
  selectedfiledetails: any;
  filename: any;
  wave: wavesurfer = null;
  fileUploaded: boolean = false;

  transcription: String = '';
  alert: String = '';
  summary: String = '';
  disposition: String = '';
  sentiment: String = '';

  alertKeys = [];

  agentname: String;
  callid: String;
  custname: String;
  date: any;
  category : String;

  afterUpload: boolean = false;
  showalert: boolean = false;
  alertdata: string;

  Block: boolean = true;
  regexUrls: any = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/gi
  regex = new RegExp(this.regexUrls);

  minimizedArray = [];
  selectedTransIndex: any;
  selectedHistoryIndex: any;
  selectedsummaryIndex: any;
  selectedalertIndex: any;
  alertStyle: boolean = false;

  transcribeAlldata: string[] = [];
  claimIdcalled: boolean = false;
  listofalerts: any[] = [];
  lastTransKey: String = '';
  transcomplete: boolean = false;

  dispupload = false;

  All_cards = [
    {
      title: 'Customer Info',
      id: 1,
      pos: 0,
      aline: 'element1',
      height: 225,
      width: 440,
      top: 0,
      left: 0,
      state: 1,
      arrow: { u: '', d: '', l: '', r: '', }
    },
    {
      title: 'Call disposition',
      id: 2,
      pos: 1,
      aline: 'element2',
      height: 225,
      width: 440,
      top: 230,
      left: 0,
      state: 1,
      arrow: { u: '', d: '', l: '', r: '', }
    },
    {
      title: 'Transcription',
      id: 3,
      pos: 2,
      aline: 'element3',
      height: 225,
      width: 440,
      top: 0,
      left: 463,
      state: 1,
      arrow: { u: '', d: '', l: '', r: '', }

    },
    {
      title: 'Call Sentiment',
      id: 4,
      pos: 3,
      aline: 'element4',
      height: 225,
      width: 440,
      top: 230,
      left: 463,
      state: 1,
      arrow: { u: '', d: '', l: '', r: '', }
    }, {
      title: 'Alerts',
      id: 5,
      pos: 4,
      aline: 'element5',
      height: 225,
      width: 440,
      top: 0,
      left: 911,
      state: 1,
      arrow: { u: '', d: '', l: '', r: '', }
    },
    {
      title: 'Call Summary',
      id: 6,
      pos: 5,
      aline: 'element6',
      height: 225,
      width: 440,
      top: 230,
      left: 911,
      state: 1,
      arrow: { u: '', d: '', l: '', r: '', }
    }
  ];

  summary_card_data: any;
  disposition_card_data: any;
  summarycarddata = 'asdfgfb';
  // radioTocalldisposition: boolean = false;
  dispositionVal: string;
  currentDate: any;
  summaryCurrentDatetime = false;

  category_select :string;
  calldisposition_dropdown_select :string;

CRM_ID='';

alertdatad='{"alert": {"data": [{"name": "Repeat Caller","message": "Repeat caller is found","icon": "critical",'+
        '"type": "Agent Coaching"},{"name": "Upselling","message": "Inform customer about hotel booking","icon": "information","type": "Agent Information"'
        +'},{"name": "Credit limit","message": "Inform credit limit to the customer","icon": "warn","type": "Agent Alert"}]},"callMeta": {}}';
  OrgName: any;
  categorylist: any;


  constructor(private realservice: RealserviceService,private fb: FormBuilder,
    private _notificationService: PushnotificationsService,
    private cdr: ChangeDetectorRef, public dialog: MatDialog) {

      
    this.realservice.sessiondetails().subscribe((data : any) => {
      console.log("sessiondeatils",data);
      this.CRM_ID = data.crmId;
      this.OrgName = data.orgName;
      this.realservice.ListofCategories(this.OrgName).subscribe((value : any) => {
        console.log("ListofCategories",value);
        value.push('abc');
        value.push('def');
        this.category_select=value[0];
        this.categorylist = value;       
      })
    })

   


    this._notificationService.requestPermission();

    this.realservice.componentMethodMetaData$.subscribe(
      (res) => {
        console.log('Meta Data ', res);
        let recData = JSON.parse(res.body);
        let actdata = recData.callMeta;
   
        this.agentname= actdata.agent;
        this. callid = actdata.callId;
        this.custname = actdata.customer;
        this.category  = actdata.category;

      }
    );


    this.realservice.componentMethodCalledAlert$.subscribe(
      (res) => {

        console.log('alert received ',res);
        let recData = JSON.parse(res.body);
                let actdata = recData.alert.alerts;
        
                actdata.forEach(alrt => {

                  if (!this.alertKeys.includes(alrt.name)) {

                    let obj = this.alertsmap.find(o => o.type === alrt.icon);
  
                    this.alert =this.alert+ '<b>'+alrt.type+': </b> '+obj.font+' '+alrt.message+'<br>';

                    this.alertKeys.push(alrt.name);

                    document.getElementById("alertdiv").style.border = "2px solid red";
                    let highlight = setTimeout(() => {
                      document.getElementById("alertdiv").style.border = "2px solid silver";
                      clearInterval(highlight);
                    }, 1500);
                  }
                 });

        // let keys = Object.keys(actdata);
        // for (let i = 0; i < keys.length; i++) {
        //   console.log('keys ', keys[i]);
        //   if (actdata[keys[i]] != null) {
        //     if (!this.alertKeys.includes(keys[i])) {
        //       this.alert = this.alert + '<b>' + keys[i] + '</b> : ' + actdata[keys[i]] + '<br>';
        //       this.alertKeys.push(keys[i]);
           
        //     }
        //   }
        // }


    
     

              // document.getElementById("alertdiv").style.border = "2px solid red";
              // let highlight = setTimeout(() => {
              //   document.getElementById("alertdiv").style.border = "2px solid silver";
              //   clearInterval(highlight);
              // }, 1500);

      }
    );

    this.realservice.componentErrorCalledCallTranscription$.subscribe(
      (res) => {
        console.log('Transcript Data ', res);
        let recData = JSON.parse(res.body);
        let actdata = recData.transcript.data;
        this.addTranscriptionText(actdata);

      }
    );

    this.realservice.componentMethodCalledSummary$.subscribe(
      (res) => {
        console.log('Summary Data ', res);
        let recData = JSON.parse(res.body);
        let actdata = recData.summary.data;
        if (actdata != null) {
          this.summary = this.summary + actdata;
          // this.summarycarddata = this.summary;
          // this.openDialogTest('summary');
        }
      }
    );

    this.realservice.componentErrorCalledDisposition$.subscribe(
      (res) => {
        console.log('dispositionosition data ', res);
        let recData = JSON.parse(res.body);
        let actdata = recData.callDisposition.data;
        let keys = Object.keys(actdata);
        for (let i = 0; i < keys.length; i++) {
          // {"callMeta":null,"transcript":null,"alert":null,"summary":null,"callDisposition":{"data":{"Status":"Booked","Issue Type":"Sea View","Call Type":"Hotel Booking","Service Type":"Deluxe"}},"sentiment":null}
          console.log('keys ', keys[i]);
          if (actdata[keys[i]] != null) {
            this.disposition = this.disposition + '<b>' + keys[i] + '</b> : ' + actdata[keys[i]] + '<br>';
          }
        }

        this.dispositionEditclicked = true;
      }
    );

    this.realservice.componentErrorCalledCallSentiment$.subscribe(
      (res) => {
        console.log('Sentiment data', res);
        let recData = JSON.parse(res.body);
        let actdata = recData.sentiment.data;
        let keys = Object.keys(actdata);
        for (let i = 0; i < keys.length; i++) {
          console.log('keys ', keys[i]);
          if (actdata[keys[i]] != null) {
            this.sentiment = this.sentiment + '<b>' + keys[i] + '</b> : ' + actdata[keys[i]] + '<br>';
          }
        }
      }
    );
  }

  checkforvalue(){
      console.log('sel val ',this.category_select);
  }

  ngOnInit() {

    // this.patientCategory = this.fb.group({
		// 	patientCategory: [null, Validators.required]
    // });
    
    // const toSelect = this.seasons.find(c => c.id);
    // this.patientCategory.get('patientCategory').setValue(toSelect);

    console.log('trans data ', this.transcribeAlldata);
    setInterval(() => {
      if (this.transcribeAlldata != undefined)
        if (this.transcribeAlldata[0] != undefined) {
          let nowdata = this.transcribeAlldata[0];
          if (nowdata.match(/<b>(.*?)<\/b>/gi)) {
            if (this.transcription.length > 0)
              this.transcription = this.transcription + '<br>' + nowdata;
            else
              this.transcription = this.transcription + '' + nowdata;
          } else {
            if (nowdata == '.')
              this.transcription = this.transcription + '' + nowdata;
            else
              this.transcription = this.transcription + ' ' + nowdata;
          }
          this.transcribeAlldata.shift();
        }
    }, 190);
  }
alertsmap =[{type:'information',font:'<i data-toggle="tooltip" data-placement="top" title="Information" class="fa fa-bell" aria-hidden="true"></i>'},
{type:'critical',font:'<i data-toggle="tooltip" data-placement="top" title="Critical" class="fa fa-exclamation-triangle" aria-hidden="true"></i>'},
{type:'normal',font:'<i data-toggle="tooltip" data-placement="top" title="Normal" class="fa fa-exclamation-circle" aria-hidden="true"></i>'}];
  checkalert(){
      let recData = JSON.parse(this.alertdatad);
        let actdata = recData.alert.data;

        actdata.forEach(alrt => {

          let obj = this.alertsmap.find(o => o.type === alrt.icon);

          this.alert =this.alert+ '<b>'+alrt.type+': </b> '+obj.font+' (' + alrt.name+') '+alrt.message+'<br>';
        });

       // let keys = Object.keys(actdata);
        // for (let i = 0; i < keys.length; i++) {
        //   console.log('keys ', keys[i]);
        //   if (actdata[keys[i]] != null) {
        //     if (!this.alertKeys.includes(keys[i])) {
        //       this.alert = this.alert + '<b>' + keys[i] + '</b> : ' + actdata[keys[i]] + '<br>';
        //       this.alertKeys.push(keys[i]);
        //       document.getElementById("alertdiv").style.border = "2px solid red";
        //       let highlight = setTimeout(() => {
        //         document.getElementById("alertdiv").style.border = "2px solid silver";
        //         clearInterval(highlight);
        //       }, 2000);
        //     }
        //   }
        // }
     

        document.getElementById("alertdiv").style.border = "2px solid red";
        let highlight = setTimeout(() => {
          document.getElementById("alertdiv").style.border = "2px solid silver";
          clearInterval(highlight);
        }, 2000);
  }


  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }


  addTranscriptionText(data) {
    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      if (data[keys[i]] != null) {
        let receivedarray = data[keys[i]];

        if (this.lastTransKey != keys[i]) {
          this.lastTransKey = keys[i];
          this.transcribeAlldata.push('<b>' + keys[i] + ' : </b>');
        }
        for (let k = 0; k < receivedarray.length; k++)
          this.transcribeAlldata.push(receivedarray[k]);
      }
    }
  }

  applyAnchor(data) {
    let matches = data.match(this.regex);
    let dat = data.replace(matches, '<a target="_blank" href="' + matches + '">' + matches + "</a>");
    return dat;
  }

  onFileSelected(event: any) {

    console.log("Inside File set");

    if (event.target.files.length > 0) {
      if (this.fileUploaded) {
        this.removeFile();

        this.fileUploaded = true;
        this.filename = event.target.files[0].name;
        this.selectedfiledetails = event.target.files[0];
        this.realservice.setFile(this.selectedfiledetails);

        this.wavfileUploded();

      }
      else {
        this.fileUploaded = true;
        this.dispupload = true;
        this.filename = event.target.files[0].name;
        this.selectedfiledetails = event.target.files[0];
        this.realservice.setFile(this.selectedfiledetails);

        console.log("File set");
      }
    }
  }

  dataObtained(data) {

    this.wave.load(URL.createObjectURL(this.selectedfiledetails));
    this.wave.on('ready', () => {
      this.wave.play();
    });
    this.callid = data.call;
    this.agentname = data.agent;
    this.custname = data.customer;
    this.date = new Date();
  }

  wavfileUploded() {
    this.afterUpload = true;
    this.realservice._connect(this.CRM_ID, this.OrgName);
    // disappear upload
    this.dispupload = false;

    // for diaplaying modal
    //  this.showalert = true;
    //this.alertdata = "Please Upload File First"
  }


  removeFile() {

    this.fileUploaded = false;

    this.realservice._disconnect();

    this.selectedfiledetails = "";
    this.filename = "";

    this.transcribeAlldata = [];
    this.lastTransKey = '';

    this.transcription = "";
    this.alert = "";
    this.summary = "";
    this.disposition = "";
    this.sentiment = "";

    this.callid = "";
    this.custname = "";
    this.date = "";
    this.agentname = "";

    this.afterUpload = false;

    //this.wave.destroy();
    //this.wave = null;
    // this.wave.empty();
  }

  swapContent(index) {
    if (this.count == 1) {
      this.copyindex = index;
      this.count = this.count + 1;
    } else if (this.count == 2) {
      if (this.copyindex === index) {
        this.copyindex = '';
        this.count = this.count - 1;
      } else {
        let title = this.All_cards[index].title;
        let id = this.All_cards[index].id;
        this.All_cards[index].title = this.All_cards[this.copyindex].title
        this.All_cards[index].id = this.All_cards[this.copyindex].id
        this.All_cards[this.copyindex].title = title;
        this.All_cards[this.copyindex].id = id;
        this.count = this.count - 1;
      }
    }
    console.log(this.All_cards)
  }

  minimize(data, index) {
    console.log(data, index)
    if (this.All_cards[index].state == 1) {
      this.positions[this.All_cards[index].pos] = true;
    }
    else if (this.All_cards[index].state == 2) {
      this.positions[this.All_cards[index].pos] = true;
      this.positions[this.All_cards[index].pos + 1] = true;
    }
    else if (this.All_cards[index].state == 3) {
      this.positions[this.All_cards[index].pos] = true;
      this.positions[this.All_cards[index].pos + 2] = true;
    }
    else {
      this.positions[2] = true;
      this.positions[3] = true;
      this.positions[4] = true;
      this.positions[5] = true;
    }
    this.minimizedArray.push(this.All_cards[index]);
    this.All_cards.splice(index, 1);

    console.log('ghb ', this.positions);
    this.checkforBrackets();
    // console.log('ghb ', this.positions);

  }

  checkforBrackets() {
    if (this.All_cards[1].state == 1) {
      if (this.positions[3])
        this.All_cards[1].arrow.r = this.ArrowRight;
      else this.All_cards[1].arrow.r = '';
    } else {
      if (!this.positions[3])
        this.All_cards[1].arrow.r = this.ArrowLeft;
      else this.All_cards[1].arrow.r = '';
    }


    for (let i = 2; i < this.All_cards.length; i++) {
      if (this.All_cards[i].state == 1) {
        if (this.All_cards[i].pos == 2) {
          if (this.positions[3])
            this.All_cards[i].arrow.d = this.ArrowDown;
          else
            this.All_cards[i].arrow.d = '';
          if (this.positions[4])
            this.All_cards[i].arrow.r = this.ArrowRight;
          else
            this.All_cards[i].arrow.r = '';
        }
        else if (this.All_cards[i].pos == 3) {
          if (this.positions[2])
            this.All_cards[i].arrow.u = this.ArrowUp;
          else
            this.All_cards[i].arrow.u = '';
          if (this.positions[5])
            this.All_cards[i].arrow.r = this.ArrowRight;
          else
            this.All_cards[i].arrow.r = '';
        }
        else if (this.All_cards[i].pos == 4) {
          if (this.positions[5])
            this.All_cards[i].arrow.d = this.ArrowDown;
          else
            this.All_cards[i].arrow.d = '';
          if (this.positions[2])
            this.All_cards[i].arrow.l = this.ArrowLeft;
          else
            this.All_cards[i].arrow.l = '';
        }
        else {
          if (this.positions[4])
            this.All_cards[i].arrow.u = this.ArrowUp;
          else
            this.All_cards[i].arrow.u = '';
          if (this.positions[3])
            this.All_cards[i].arrow.l = this.ArrowLeft;
          else
            this.All_cards[i].arrow.l = '';
        }
      }
      else if (this.All_cards[i].state == 2) {
        if (this.All_cards[i].pos == 2) {
          if (this.positions[4] && this.positions[5])
            this.All_cards[i].arrow.r = this.ArrowRight;
          else
            this.All_cards[i].arrow.r = '';
        }
        else {
          if (this.positions[2] && this.positions[3])
            this.All_cards[i].arrow.l = this.ArrowLeft;
          else
            this.All_cards[i].arrow.l = '';
        }
      }
      else if (this.All_cards[i].state == 3) {
        if (this.All_cards[i].pos == 2) {
          if (this.positions[3] && this.positions[5])
            this.All_cards[i].arrow.d = this.ArrowDown;
          else
            this.All_cards[i].arrow.d = '';
        }
        else {
          if (this.positions[2] && this.positions[4])
            this.All_cards[i].arrow.u = this.ArrowUp;
          else
            this.All_cards[i].arrow.u = '';
        }
      }
    }
  }

  maximize(posty, index) {
    console.log('max ', posty, index);
    let l = [463, 463, 911, 911];
    let t = [0, 230, 0, 230];
    posty.height = 225;
    posty.width = 440;
    posty.state = 1;
    posty.arrow = { u: '', d: '', l: '', r: '', };
    for (let i = 2; i < 6; i++) {
      posty.pos = i;
      posty.top = t[i - 2];
      posty.left = l[i - 2];
      if (this.positions[i]) {
        this.All_cards.splice(i, 0, posty);
        this.positions[i] = false;
        break;
      }
    }
    this.checkforBrackets();
    this.minimizedArray.splice(index, 1);
  }

  resize(i, d) {
    if (d == 'd') {
      if (this.All_cards[i].state == 1) {
        this.All_cards[i].height = 455;
        this.All_cards[i].state = 2;
        this.All_cards[i].arrow.d = this.ArrowUp;
        if (this.All_cards[i].pos == 2)
          this.positions[3] = false;
        else
          this.positions[5] = false;
      }
      else if (this.All_cards[i].state == 2) {
        this.All_cards[i].height = 225;
        this.All_cards[i].state = 1;
        this.All_cards[i].arrow.d = this.ArrowDown;
        if (this.All_cards[i].pos == 2)
          this.positions[3] = true;
        else
          this.positions[5] = true;
      }
      else if (this.All_cards[i].state == 3) {
        this.All_cards[i].height = 455;
        this.All_cards[i].state = 4;
        this.All_cards[i].arrow.d = this.ArrowUp;
        this.positions[3] = false;
        this.positions[5] = false;
      }
      else {
        this.All_cards[i].height = 225;
        this.All_cards[i].arrow.d = this.ArrowDown;
        this.All_cards[i].state = 3;
        this.positions[3] = true;
        this.positions[5] = true;
      }
    }
    else if (d == 'u') {
      this.All_cards[i].top = 0;
      this.All_cards[i].height = 455;
      this.All_cards[i].state = 2;
      this.All_cards[i].arrow.d = this.ArrowUp;
      if (this.All_cards[i].pos == 5) {
        this.All_cards[i].pos = 4;
        this.positions[4] = false;
      }
      else {
        this.All_cards[i].pos = 2;
        this.positions[2] = false;
      }
    }
    else if (d == 'r') {
      if (this.All_cards[i].state == 1) {
        console.log('if case ');

        this.All_cards[i].arrow.r = this.ArrowLeft;
        this.All_cards[i].width = 888;
        this.All_cards[i].state = 3;
        if (this.All_cards[i].pos == 2)
          this.positions[4] = false;
        else if (this.All_cards[i].pos == 3)
          this.positions[5] = false;
        // for call dispositionosition special case
        else this.positions[3] = false;

      } else if (this.All_cards[i].state == 3) {
        console.log('else case ');
        this.All_cards[i].width = 440;
        this.All_cards[i].state = 1;
        this.All_cards[i].arrow.r = this.ArrowRight;
        if (this.All_cards[i].pos == 2)
          this.positions[4] = true;
        else if (this.All_cards[i].pos == 3)
          this.positions[5] = true;
        // for call dispositionosition special case
        else this.positions[3] = true;

      }
      else if (this.All_cards[i].state == 2) {
        this.All_cards[i].width = 888;
        this.All_cards[i].state = 4;
        this.All_cards[i].arrow.r = this.ArrowLeft;
        this.positions[4] = false;
        this.positions[5] = false;
      }
      else {
        this.All_cards[i].width = 440;
        this.All_cards[i].arrow.r = this.ArrowRight;
        this.All_cards[i].state = 2;
        this.positions[4] = true;
        this.positions[5] = true;
      }
    }
    else {
      this.All_cards[i].left = 463;
      this.All_cards[i].width = 888;
      this.All_cards[i].state = 3;
      this.All_cards[i].arrow.r = this.ArrowLeft;
      if (this.All_cards[i].pos == 4) {
        this.All_cards[i].pos = 2;
        this.positions[2] = false;
      }
      else {
        this.All_cards[i].pos = 3;
        this.positions[3] = false;
      }
    }
    console.log('pos ', this.positions);
    this.checkforBrackets();

  }

  openDialog() {
    // const dialogConfig = new MatDialogConfig();
    let initialdialogRef = this.dialog.open(InitialdialogComponent, {
      data: 'Are you fine with Automated Call Summary'
    });
    initialdialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result === 'Need Changes') {
        this.summaryfieldchanges('Sumpop');
        let summarydialogRef = this.dialog.open(SummarydialogComponent, {
          disableClose: true,
          data: this.summarycarddata
        })
        summarydialogRef.afterClosed().subscribe(result => {
          if (result == 'close') {
            this.summaryfieldchanges('Sumpush');
          }
        })
        const dialogSubmitSubscription =
          summarydialogRef.componentInstance.submitClicked.subscribe(result => {
            console.log("result", result)
            // this.summary = result;
            this.summaryfieldchanges('Sumpush');
            dialogSubmitSubscription.unsubscribe();
            let initalcallrepeat = this.dialog.open(InitialdialogComponent,
              {
                data: 'Are you fine with Automated Call Disposition'
              });
            initalcallrepeat.afterClosed().subscribe(result => {
              if (result == 'Need Changes') {
                this.summaryfieldchanges('Dispop');
                let callDisposition = this.dialog.open(DisposotiondialogComponent, {
                  data: [
                    { header: 'Call Type', items: ['Hotel Booking', 'Insurance'], select: 'Closed' },
                    { header: 'Service', items: ['Deluxe', 'Normal', 'Benefit Verification'], select: 'Closed' },
                    {
                      header: 'Issue', items: ['City view', 'Sea View', 'Multi-Bed',
                        'Single Bed', 'Claim'], select: 'Closed'
                    },
                    { header: 'Status', items: ['Closed', 'Booked'], select: 'Closed' }
                  ]
                })
                const dialogSubmitdispostion =
                  callDisposition.componentInstance.submitClicked.subscribe(result => {
                    console.log('Dis result', result)
                    this.summaryfieldchanges('Dispush');
                    dialogSubmitdispostion.unsubscribe();
                  })
              }
            })
          });
      }
    });
  }

  summaryfieldchanges(value) {
    let calsumid = 6; let indexcal = this.All_cards.map(val => val.id).indexOf(calsumid);
    let caldisid = 2; let indexdis = this.All_cards.map(val => val.id).indexOf(caldisid);
    if (value == 'Sumpop') {
      this.summary_card_data = this.All_cards[indexcal];
      this.All_cards.splice(indexcal, 1);
    } else if (value == 'Sumpush') {
      this.All_cards.push(this.summary_card_data);
    } else if (value == 'Dispop') {
      this.disposition_card_data = this.All_cards[indexdis];
      this.All_cards.splice(indexdis, 1);
    } else if (value == 'Dispush') {
      this.All_cards.push(this.disposition_card_data);
    }
  }
  openDialogTest(cardlabel) {
    this.summarycardbtns = true;
  }

  EditApiValue(editfield) {
    if (editfield === 'editSummary') {
      this.summaryEditclicked = true;
      this.copiedsummarydata = this.summary;
    } else {
      console.log("dispo", this.disposition)
      this.blockdropdown = false;
    }
  }

  SubmitValue(submitfield) {
    if (submitfield === "submitSummary") {
      this.currentDate = Date.now();
      this.summarytimestamplabel = true;
      this.summaryEditclicked = false;
      this.summary = this.copiedsummarydata;
      console.log("copiedsummarydata", this.copiedsummarydata)
      this.realservice.saveSummary(this.summary, this.callid)
    }
    else {
      this.currentDate = Date.now();
      this.distimestamplabel = true;
      console.log("dispFilterVal", this.dispFilterVal);
      let collection = [];
      let data = [
        { id: '1', header: 'Call Type' },
        { id: '2', header: 'Service' },
        { id: '3', header: 'Issue' },
        { id: '4', header: 'Status' }
      ]
      data.filter(val => {
        if (val.id == this.dispFilterVal[0]) {
          collection.push(val.header, this.dispFilterVal[1])
        }
      })
      this.dispFilterVal = [];
      console.log('collect', collection)
    }
  }

  // call disposition dropdown selection data
  filterdataselected(event) {
    this.seasons.forEach(val => {
      val.items.filter(chck => {
        if (event.value === chck)
          this.dispFilterVal.push(val.id, event.value)
      })
    })
    console.log("data", this.dispFilterVal)
  }

}